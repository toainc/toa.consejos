(function () {
	'use strict';

	/**
	 * The toaconsejosApp.toggleComponent module
	 * @module toaconsejosApp.toggleComponent
	 * @name ToggleComponent
	 */

	angular
		.module('toaconsejosApp.toggleComponent', []);
})();
