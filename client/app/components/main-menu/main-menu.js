/**
 * @ngdoc overview
 * @name mainMenu
 * @requires toaconsejosApp.lodash
 * @requires toaconsejosApp.auth
 * @description
 * The `toaconsejosApp.mainMenu` module which provides:
 *
 * - {@link mainMenu.controller:MainMenuController MainMenuController}
 * - {@link mainMenu.service:mainMenu mainMenu-service}
 */

(function () {
	'use strict';

	angular.module('toaconsejosApp.mainMenu', [
		'toaconsejosApp.lodash',
		'toaconsejosApp.auth'
	])

})();
