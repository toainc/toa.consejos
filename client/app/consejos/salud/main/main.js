(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.salud.main module
	 * and configure it.
	 *
	 * @requires ui.router
	 * @requires toaconsejosApp.mainMenu
	 */

	angular
		.module('toaconsejosApp.salud.main', [
			'ui.router',
			'toaconsejosApp.mainMenu'
		])
		.config(configSaludMainRoutes);

	// inject configSaludMainRoutes dependencies
	configSaludMainRoutes.$inject = ['$stateProvider', 'mainMenuProvider'];

	/**
	 * Route configuration function configuring the passed $stateProvider.
	 * Register the salud.main state with the list template for the
	 * 'main' view paired with the SaludMainController as 'main'.
	 *
	 * @param {$stateProvider} $stateProvider - The state provider to configure
	 * @param {mainMenuProvider} mainMenuProvider - The service to pass navigation information to
	 */
	function configSaludMainRoutes($stateProvider, mainMenuProvider) {
		// The main state configuration
		var mainState = {
			name: 'salud.main',
			parent: 'salud',
			url: '/',
			authenticate: true,
			role: 'user',
			views: {
				'@salud': {
					templateUrl: 'app/consejos/salud/main/main.html',
					controller: 'SaludMainController',
					controllerAs: 'main'
				}
			}
		};

		$stateProvider.state(mainState);

		mainMenuProvider.addMenuItem({
			name: 'Salud',
			state: mainState.name,
			role: 'user'
		});
	}

})();
