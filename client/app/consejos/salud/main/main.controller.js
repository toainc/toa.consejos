(function () {
	'use strict';

	/**
	 * Register the list controller as SaludMainController
	 */

	angular
		.module('toaconsejosApp.salud.main')
		.controller('SaludMainController', SaludMainController);

	// add SaludMainController dependencies to inject
	SaludMainController.$inject = ['$state'];

	/**
	 * SaludMainController constructor
	 */
	function SaludMainController($state) {
		var vm = this;
		// switch to the list state
		vm.showList = showList;

		/**
		 * Activate the salud.list state
		 */
		function showList() {
			$state.go('salud.list');
		}
	}

})();
