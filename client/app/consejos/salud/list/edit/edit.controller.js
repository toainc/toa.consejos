/**
 * @ngdoc controller
 * @name toaconsejosAppsalud.list.edit.controller:SaludEditController
 * @description
 * Controller of the salud edit page of the admin section
 */

(function () {
	'use strict';

	/**
	 * Register the edit controller as SaludEditController
	 */

	angular
		.module('toaconsejosApp.salud.list.edit')
		.controller('SaludEditController', SaludEditController);

	/**
	 * @ngdoc function
	 * @name toaconsejosAppsalud.list.edit.provider:SaludEditController
	 * @description
	 * Provider of the {@link toaconsejosAppsalud.list.edit.controller:SaludEditController SaludEditController}
	 * @param {Service} $state The state service to use
	 * @param {Service} $stateParams The stateParams service to use
	 * @param {Service} $mdDialog The dialog service to use
	 * @param {Service} Toast The Toast service to use
	 * @param {Service} SaludService The SaludService to use
	 * @param {Resource} salud The salud data to use
	 */

	SaludEditController.$inject = ['$state', '$stateParams', '$mdDialog', 'Toast', 'SaludService', 'salud'];

	function SaludEditController($state, $stateParams, $mdDialog, Toast, SaludService, salud) {
		var vm = this;

		// defaults
		vm.salud = angular.copy(salud, vm.salud);
		vm.displayName = salud.name;

		// view model bindings
		vm.update = update;
		vm.remove = remove;
		vm.goBack = goBack;
		vm.showList = showList;

		/**
		 * Open the detail state with the current salud
		 *
		 */
		function goBack() {
			$state.go('^.detail', {id: vm.salud._id});
		}

		/**
		 * Open the salud list state
		 *
		 */
		function showList() {
			$state.go('^');
		}
		/**
		 * Updates a salud by using the SaludService save method
		 * @param {Form} [form]
		 */
		function update(form) {
			// refuse to work with invalid data
			if (!vm.salud._id || form && !form.$valid) {
				return;
			}

			SaludService.update(vm.salud)
				.then(updateSaludSuccess)
				.catch(updateSaludCatch);

			function updateSaludSuccess(updatedSalud) {
				// update the display name after successful save
				vm.displayName = updatedSalud.name;
				Toast.show({text: 'Salud ' + vm.displayName + ' updated'});
				if (form) {
					form.$setPristine();
				}
			}

			function updateSaludCatch(err) {
				Toast.show({
					type: 'warn',
					text: 'Error while updating Salud ' + vm.displayName,
					link: {state: $state.$current, params: $stateParams}
				});

				if (form && err) {
					form.setResponseErrors(err.data);
				}
			}
		}

		/**
		 * Show a dialog to ask the salud if she wants to delete the current selected salud.
		 * @param {AngularForm} form - The form to pass to the remove handler
		 * @param {$event} ev - The event to pass to the dialog service
		 */
		function remove(form, ev) {
			var confirm = $mdDialog.confirm()
				.title('Delete salud ' + vm.displayName + '?')
				.content('Do you really want to delete salud ' + vm.displayName + '?')
				.ariaLabel('Delete salud')
				.ok('Delete salud')
				.cancel('Cancel')
				.targetEvent(ev);

			$mdDialog.show(confirm)
				.then(performRemove);

			/**
			 * Removes a salud by using the SaludService remove method
			 * @api private
			 */
			function performRemove() {
				SaludService.remove(vm.salud)
					.then(deleteSaludSuccess)
					.catch(deleteSaludCatch);

				function deleteSaludSuccess() {
					Toast.show({type: 'success', text: 'Salud ' + vm.displayName + ' deleted'});
					vm.showList();
				}

				function deleteSaludCatch(err) {
					Toast.show({
						type: 'warn',
						text: 'Error while deleting salud ' + vm.displayName,
						link: {state: $state.$current, params: $stateParams}
					});

					if (form && err) {
						form.setResponseErrors(err, vm.errors);
					}
				}
			}
		}
	}
})();
