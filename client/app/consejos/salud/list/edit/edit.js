(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.salud.list.edit module
	 * and configure it.
	 *
	 * @requires 'ui.router',
	 * @requires 'ngMaterial',
	 * @requires toaconsejosApp.mongooseError
	 * @requires toaconsejosApp.salud.service
	 */

	angular
		.module('toaconsejosApp.salud.list.edit', [
			'ui.router',
			'ngMaterial',
			'toaconsejosApp.mongooseError',
			'toaconsejosApp.salud.service'
		])
		.config(configureSaludListEdit);

	// inject configSaludListEdit dependencies
	configureSaludListEdit.$inject = ['$stateProvider'];

	/**
	 * Route configuration function configuring the passed $stateProvider.
	 * Register the salud.list.edit state with the edit template
	 * paired with the SaludEditController as 'edit' for the
	 * 'detail@salud.list' view.
	 * 'salud' is resolved as the salud with the id found in
	 * the state parameters.
	 *
	 * @param {$stateProvider} $stateProvider - The state provider to configure
	 */
	function configureSaludListEdit($stateProvider) {
		// The edit state configuration.
		var editState = {
			name: 'salud.list.edit',
			parent: 'salud.list',
			url: '/edit/:id',
			authenticate: true,
			role: 'user',
			onEnter: onEnterSaludListEdit,
			views: {
				'detail@salud.list': {
					templateUrl: 'app/consejos/salud/list/edit/edit.html',
					controller: 'SaludEditController',
					controllerAs: 'edit',
					resolve: {salud: resolveSaludFromArray}
				}
			}
		};

		$stateProvider.state(editState);
	}

	// inject onSaludListEditEnter dependencies
	onEnterSaludListEdit.$inject = ['$timeout', 'ToggleComponent'];

	/**
	 * Executed when entering the salud.list.detail state. Open the component
	 * registered with the component id 'salud.detailView'.
	 *
	 * @params {$timeout} $timeout - The $timeout service to wait for view initialization
	 * @params {ToggleComponent} ToggleComponent - The service to toggle the detail view
	 */
	function onEnterSaludListEdit($timeout, ToggleComponent) {
		$timeout(showDetails, 0, false);

		function showDetails() {
			ToggleComponent('salud.detailView').open();
		}
	}

	// inject resolveSaludDetailRoute dependencies
	resolveSaludFromArray.$inject = ['saluds', '$stateParams', '_'];

	/**
	 * Resolve dependencies for the salud.list.edit state. Get the salud
	 * from the injected Array of saluds by using the '_id' property.
	 *
	 * @params {Array} saluds - The array of saluds
	 * @params {Object} $stateParams - The $stateParams to read the salud id from
	 * @params {Object} _ - The lodash service to find the requested salud
	 * @returns {Object|null} The salud whose value of the _id property equals $stateParams._id
	 */
	function resolveSaludFromArray(saluds, $stateParams, _) {
		//	return Salud.get({id: $stateParams.id}).$promise;
		return _.find(saluds, {'_id': $stateParams.id});
	}

})();
