(function () {
	'use strict';

	/**
	 * Register the edit controller as SaludDetailController
 	 */

	angular
		.module('toaconsejosApp.salud.list.detail')
		.controller('SaludDetailController', SaludDetailController);

	// add SaludDetailController dependencies to inject
	SaludDetailController.$inject = ['$state', 'salud'];

	/**
	 * SaludDetailController constructor
	 */
	function SaludDetailController($state, salud) {
		var vm = this;

		// the current salud to display
		vm.salud = salud;
		// switch to the edit state
		vm.edit = edit;
		// switch to the parent state
		vm.goBack = goBack

		/**
		 * Open the edit state with the current salud
		 *
		 */
		function edit() {
			$state.go('^.edit', {'id': vm.salud._id});
		}

		/**
		 * Return to the parent state
		 *
		 */
		function goBack() {
			$state.go('^');
		}
	}
})();
