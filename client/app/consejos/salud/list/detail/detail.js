(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.salud.list.detail submodule
	 * and configure it.
	 *
   * @requires ui.router
	 * @requires angularMoment
	 */

	angular
		.module('toaconsejosApp.salud.list.detail', [
			'ui.router',
			'angularMoment'
		])
		.config(configureSaludListDetail);

	// inject configSaludRoutes dependencies
	configureSaludListDetail.$inject = ['$stateProvider'];

	/**
	 * Route configuration function configuring the passed $stateProvider.
	 * Register the 'salud.detail' state with the detail template
	 * paired with the SaludDetailController as 'detail' for the
	 * 'sidenav' sub view.
	 * 'salud' is resolved as the salud with the id found in
	 * the state parameters.
	 *
	 * @param {$stateProvider} $stateProvider - The state provider to configure
	 */
	function configureSaludListDetail($stateProvider) {
		// The detail state configuration
		var detailState = {
			name: 'salud.list.detail',
			parent: 'salud.list',
			url: '/:id',
			authenticate: true,
			role: 'user',
			onEnter: onEnterSaludListDetail,
			views: {
				'detail@salud.list': {
					templateUrl: 'app/consejos/salud/list/detail/detail.html',
					controller: 'SaludDetailController',
					controllerAs: 'detail',
					resolve: {salud: resolveSaludFromArray}
				}
			}
		};

		$stateProvider.state(detailState);
	}

	// inject onSaludListDetailEnter dependencies
	onEnterSaludListDetail.$inject = ['$timeout', 'ToggleComponent'];

	/**
	 * Executed when entering the salud.list.detail state. Open the component
	 * registered with the component id 'salud.detailView'.
	 *
 	 * @params {$timeout} $timeout - The $timeout service to wait for view initialization
	 * @params {ToggleComponent} ToggleComponent - The service to toggle the detail view
	 */
	function onEnterSaludListDetail($timeout, ToggleComponent) {
		$timeout(showDetails, 0, false);

		function showDetails() {
			ToggleComponent('salud.detailView').open();
		}
	}

	// inject resolveSaludFromArray dependencies
	resolveSaludFromArray.$inject = ['saluds', '$stateParams', '_'];

	/**
	 * Resolve dependencies for the salud.detail state
	 *
	 * @params {Array} saluds - The array of saluds
	 * @params {Object} $stateParams - The $stateParams to read the salud id from
	 * @returns {Object|null} The salud whose value of the _id property equals $stateParams._id
	 */
	function resolveSaludFromArray(saluds, $stateParams, _) {
		return _.find(saluds, {'_id': $stateParams.id});
	}

})();
