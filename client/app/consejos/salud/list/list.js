(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.salud.list module
	 * and configure it.
	 * @requires ui.router
	 * @requires ngMaterial
	 * @requires toaconsejosApp.socket
	 * @requires toaconsejosApp.mainMenu,
	 * @requires toaconsejosApp.toggleComponent,
	 * @requires toaconsejosApp.salud.list.detail
	 * @requires toaconsejosApp.salud.list.edit
	 * @requires toaconsejosApp.salud.list.items
	 */

	angular
		.module('toaconsejosApp.salud.list', [
			'ngMaterial',
			'ui.router',
			'toaconsejosApp.socket',
			'toaconsejosApp.mainMenu',
			'toaconsejosApp.toggleComponent',
			'toaconsejosApp.salud.list.detail',
			'toaconsejosApp.salud.list.edit',
			'toaconsejosApp.salud.list.items'
		])
		.config(configSaludListRoutes);

	// inject configSaludListRoutes dependencies
	configSaludListRoutes.$inject = ['$stateProvider', 'mainMenuProvider'];

	/**
	 * Route configuration function configuring the passed $stateProvider.
	 * Register the salud.list state with the list template fpr the
	 * 'main' view paired with the SaludListController as 'list'.
	 *
	 * @param {$stateProvider} $stateProvider - The state provider to configure
	 */
	function configSaludListRoutes($stateProvider, mainMenuProvider) {
		// The list state configuration
		var listState = {
			name: 'salud.list',
			parent: 'salud',
			url: '/list',
			authenticate: true,
			role: 'user',
			resolve: {
				saluds:  resolveSaluds
			},
			views: {

				// target the unnamed view in the salud state
				'@salud': {
					templateUrl: 'app/consejos/salud/list/list.html',
					controller: 'SaludListController',
					controllerAs: 'list'
				},

				// target the content view in the salud.list state
				'content@salud.list': {
					templateUrl: 'app/consejos/salud/list/items/items.html',
					controller: 'SaludItemsController',
					controllerAs: 'items'
				}
			}
		};

		$stateProvider.state(listState);

		mainMenuProvider.addSubMenuItem('salud.main', {
			name: 'Salud List',
			state: listState.name
		});
	}

	// inject resolveSaluds dependencies
	resolveSaluds.$inject = ['Salud'];

	/**
	 * Resolve dependencies for the salud.list state
	 *
	 * @params {Salud} Salud - The service to query saluds
	 * @returns {Promise} A promise that, when fullfilled, returns an array of saluds
	 */
	function resolveSaluds(Salud) {
		return Salud.query().$promise;
	}

})();
