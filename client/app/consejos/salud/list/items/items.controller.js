(function () {
	'use strict';

	/**
	 * Register the list controller as SaludItemsController
	 */

	angular
		.module('toaconsejosApp.salud.list.items')
		.controller('SaludItemsController', SaludItemsController);

	// add SaludItemsController dependencies to inject
	SaludItemsController.$inject = ['$state'];

	/**
	 * SaludItemsController constructor
	 */
	function SaludItemsController($state) {
		var vm = this;

		// the selected item id
		var curSaludId = null;

		// check if this item is selected
		vm.isSelected = isSelected;
		// switch to the detail state
		vm.showInDetails = showInDetails;

		/**
		 * Check if the passed item is the current selected item
		 *
		 * @param {Object} salud - The object to check for selection
		 */
		function isSelected(salud) {
			return curSaludId === salud._id;
		}

		/**
		 * Open the detail state with the selected item
		 *
		 * @param {Object} salud - The salud to edit
		 */
		function showInDetails(salud) {
			curSaludId = salud._id;
			$state.go('salud.list.detail', {'id': curSaludId});
		}
	}

})();
