(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.salud.list.items module
	 * @requires ui.router
	 */

	angular
		.module('toaconsejosApp.salud.list.items', ['ui.router']);

})();
