(function () {
	'use strict';

	/**
	 * Register the list controller as SaludListController
	 */
	angular
		.module('toaconsejosApp.salud.list')
		.controller('SaludListController', SaludListController);

	// add SaludListController dependencies to inject
	SaludListController.$inject = ['$scope', 'socket', '$state', 'saluds', 'ToggleComponent'];

	/**
	 * SaludListController constructor
	 *
	 * @param {Object} $scope - The current scope
	 * @param {Object} socket - The socket service to register to
	 * @param {$state} $state - The $state to activate routing states on
	 * @param {Array} saluds - The list of saluds resolved for this route
	 * @param {Service} ToggleComponent - The service for switching the detail view
	 */
	function SaludListController($scope, socket, $state, saluds, ToggleComponent) {
		var vm = this;

		// the array of saluds
		vm.saluds = saluds;
		// toggle detail view
		vm.toggleDetails = toggleDetails;

		// initialize the controller
		activate();

		/**
		 * Register socket updates and unsync on scope $destroy event
		 */
		function activate() {
			socket.syncUpdates('salud', vm.saluds);
			$scope.$on('$destroy', unsyncSaludUpdates);

			function unsyncSaludUpdates() {
				socket.unsyncUpdates('salud');
			}
		}

		/**
		 * Toggle the detail view
		 */
		function toggleDetails() {
			ToggleComponent('salud.detailView').toggle();
		}
	}

})();
