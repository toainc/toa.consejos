(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.salud.service module.
	 * Register the salud resource as Salud, register the
	 * service as SaludService.
	 *
	 * @requires {toaconsejosApp.resource}
	 */
	angular
		.module('toaconsejosApp.salud.service', ['toaconsejosApp.resource'])
		.factory('Salud', Salud)
		.service('SaludService', SaludService);

	// add Salud dependencies to inject
	Salud.$inject = ['Resource'];

	/**
	 * Salud resource constructor
	 */
	function Salud($resource) {
		// factory members
		var apiURL = '/api/consejos/salud';
		// public API
		return $resource(apiURL + '/:id/:controller');
	}

	// add SaludService dependencies to inject
	SaludService.$inject = ['Salud'];

	/**
	 * SaludService constructor
	 * AngularJS will instantiate a singleton by calling "new" on this function
	 *
	 * @param {$resource} Salud The resource provided by toaconsejosApp.salud.resource
	 * @returns {Object} The service definition for the SaludService service
	 */
	function SaludService(Salud) {

		return {
			create: create,
			update: update,
			remove: remove
		};

		/**
		 * Save a new salud
		 *
		 * @param  {Object}   salud - saludData
		 * @param  {Function} callback - optional
		 * @return {Promise}
		 */
		function create(salud, callback) {
			var cb = callback || angular.noop;

			return Salud.create(salud,
				function (salud) {
					return cb(salud);
				},
				function (err) {
					return cb(err);
				}).$promise;
		}

		/**
		 * Remove a salud
		 *
		 * @param  {Object}   salud - saludData
		 * @param  {Function} callback - optional
		 * @return {Promise}
		 */
		function remove(salud, callback) {
			var cb = callback || angular.noop;

			return Salud.remove({id: salud._id},
				function (salud) {
					return cb(salud);
				},
				function (err) {
					return cb(err);
				}).$promise;
		}

		/**
		 * Create a new salud
		 *
		 * @param  {Object}   salud - saludData
		 * @param  {Function} callback - optional
		 * @return {Promise}
		 */
		function update(salud, callback) {
			var cb = callback || angular.noop;

			return Salud.update(salud,
				function (salud) {
					return cb(salud);
				},
				function (err) {
					return cb(err);
				}).$promise;
		}
	};
})();
