/**
 * @ngdoc controller
 * @name toaconsejosApp.salud.create.controller:SaludCreateController
 * @description
 * Controller of the salud create page of the admin section
 */

(function () {
	'use strict';

	/**
	 * Register the create controller as SaludCreateController
	 */

	angular
		.module('toaconsejosApp.salud.create')
		.controller('SaludCreateController', SaludCreateController);

	/**
	 * @ngdoc function
	 * @name toaconsejosApp.salud.create.provider:SaludCreateController
	 * @description
	 * Provider of the {@link toaconsejosApp.salud.create.controller:SaludCreateController SaludCreateController}
	 *
	 * @param {Service} Auth The Auth service to use
	 * @param {Service} $mdDialog The mdDialog service to use
	 * @param {Service} Salud The Salud resource
	 * @param {Service} SaludService The Salud service to use
	 * @param {Service} Toast The Toast service to use
	 * @returns {Service} {@link toaconsejosApp.salud.create.controller:SaludCreateController SaludCreateController}
	 */

	SaludCreateController.$inject = ['$mdDialog', 'Salud', 'SaludService', 'Toast'];

	function SaludCreateController($mdDialog, Salud, SaludService, Toast) {
		var vm = this;

		/**
		 * @ngdoc property
		 * @name salud
		 * @propertyOf toaconsejosApp.salud.create.controller:SaludCreateController
		 * @description
		 * The new salud data
		 *
		 * @returns {Object} The salud data
		 */
		vm.salud = new Salud();

		// view model bindings (documented below)
		vm.create = createSalud;
		vm.close = hideDialog;
		vm.cancel = cancelDialog;

		/**
		 * @ngdoc function
		 * @name createSalud
		 * @methodOf toaconsejosApp.salud.create.controller:SaludCreateController
		 * @description
		 * Create a new salud by using the SaludService create method
		 *
		 * @param {form} [form] The form to gather the information from
		 */
		function createSalud(form) {
			// refuse to work with invalid data
			if (vm.salud._id || (form && !form.$valid)) {
				return;
			}

			SaludService.create(vm.salud)
				.then(createSaludSuccess)
				.catch(createSaludCatch);

			function createSaludSuccess(newSalud) {
				Toast.show({
					type: 'success',
					text: 'Salud ' + newSalud.name + ' has been created',
					link: {state: 'salud.list.detail', params: {id: newSalud._id}}
				});
				vm.close();
			}

			function createSaludCatch(err) {
				if (form && err) {
					form.setResponseErrors(err);
				}

				Toast.show({
					type: 'warn',
					text: 'Error while creating a new Salud'
				});
			}
		}

		/**
		 * @ngdoc function
		 * @name hide
		 * @methodOf toaconsejosApp.salud.create.controller:SaludCreateController
		 * @description
		 * Hide the dialog
		 */
		function hideDialog() {
			$mdDialog.hide();
		}

		/**
		 * @ngdoc function
		 * @name cancel
		 * @methodOf toaconsejosApp.salud.create.controller:SaludCreateController
		 * @description
		 * Cancel the dialog
		 */
		function cancelDialog() {
			$mdDialog.cancel();
		}
	}
})();
