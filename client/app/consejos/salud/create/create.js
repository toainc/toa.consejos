(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.salud.create module
	 * and configure it.
	 *
	 * @requires ui.router
	 * @requires ngMessages
	 * @requires ngMaterial
	 * @requires {toaconsejosApp.mongooseError}
	 * @requires {toaconsejosApp.remoteUnique}
	 * @requires {toaconsejosApp.salud.service}
	 */

	angular
		.module('toaconsejosApp.salud.create', [
			'ui.router',
			'ngMessages',
			'ngMaterial',
			'toaconsejosApp.mongooseError',
			'toaconsejosApp.remoteUnique',
			'toaconsejosApp.salud.service'
		])
		.config(configureSaludCreateRoutes);

	// inject configSalud.CreateRoutes dependencies
	configureSaludCreateRoutes.$inject = ['$stateProvider'];

	/**
	 * Route configuration function configuring the passed $stateProvider.
	 * Register the 'salud.list.create' state. The onEnterSaludListCreateView
	 * function will be called when entering the state and open a modal dialog
	 * with the app/consejos/salud/create/create.html template loaded.
	 *
	 * @param {$stateProvider} $stateProvider - The state provider to configure
	 */
	function configureSaludCreateRoutes($stateProvider) {
		var  createListState = {
			name: 'salud.list.create',
			parent: 'salud.list',
			url: '/create',
			authenticate: true,
			role: 'user',
			onEnter: onEnterSaludListCreateView
		};

		$stateProvider.state(createListState);
	}

	/**
	 * Function that executes when entering the salud.list.create state.
	 * Open the create dialog
	 */

	onEnterSaludListCreateView.$inject = ['$rootScope', '$state', '$mdDialog'];

	function onEnterSaludListCreateView($rootScope, $state, $mdDialog) {
		var unregisterListener = $rootScope.$on('$stateChangeStart', onStateChange);

		$mdDialog.show({
			controller: 'SaludCreateController',
			controllerAs: 'create',
			templateUrl: 'app/consejos/salud/create/create.html',
			clickOutsideToClose: false
		}).then(transitionTo, transitionTo);

		/**
		 * Function executed when resolving or rejecting the
		 * dialog promise.
		 *
		 * @param {*} answer - The result of the dialog callback
		 * @returns {promise}
		 */
		function transitionTo(answer) {
			return $state.transitionTo('salud.list');
		}

		/**
		 * Function executed when changing the state.
		 * Closes the create dialog
		 */
		function onStateChange() {
			unregisterListener();
			//$mdDialog.hide();
		}
	}

})();
