(function () {
	'use strict';

	// register the controller as SaludController
	angular
		.module('toaconsejosApp.salud')
		.controller('SaludController', SaludController);

	// add SaludController dependencies to inject
	// SaludController.$inject = [];

	/**
	 * SaludController constructor. Main controller for the toaconsejosApp.salud
	 * module.
	 *
	 * @param {$scope} $scope - The scope to listen for events
	 * @param {socket.io} socket - The socket to register updates
	 */
	function SaludController() {
		// var vm = this;
	}

})();
