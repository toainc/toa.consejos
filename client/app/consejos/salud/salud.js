(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.salud module
	 * and configure it.
	 *
	 * @requires ui.router
	 * @requires ngResource
	 * @requires toaconsejosApp.salud.main
	 * @requires toaconsejosApp.salud.list
	 * @requires toaconsejosApp.salud.create
	 */
	angular
		.module('toaconsejosApp.salud', [
			'ngResource',
			'ui.router',
			'toaconsejosApp.salud.main',
			'toaconsejosApp.salud.list',
			'toaconsejosApp.salud.create'
		])
		.config(configSaludRoutes);

	// inject configSaludRoutes dependencies
	configSaludRoutes.$inject = ['$urlRouterProvider', '$stateProvider'];

	/**
	 * Route configuration function configuring the passed $stateProvider.
	 * Register the abstract salud state with the salud template
	 * paired with the SaludController as 'index'.
	 * The injectable 'saluds' is resolved as a list of all saluds
	 * and can be injected in all sub controllers.
	 *
	 * @param {$stateProvider} $stateProvider - The state provider to configure
	 */
	function configSaludRoutes($urlRouterProvider, $stateProvider) {
		// The salud state configuration
		var saludState = {
			name: 'salud',
			url: '/consejos/salud',
			abstract: true,
			templateUrl: 'app/consejos/salud/salud.html',
			controller: 'SaludController',
			controllerAs: 'index'
		};

		$urlRouterProvider.when('/salud', '/salud/');
		$stateProvider.state(saludState);
	}

})();
