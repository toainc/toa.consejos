(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.nutricion module
	 * and configure it.
	 *
	 * @requires ui.router
	 * @requires ngResource
	 * @requires toaconsejosApp.nutricion.main
	 * @requires toaconsejosApp.nutricion.list
	 * @requires toaconsejosApp.nutricion.create
	 */
	angular
		.module('toaconsejosApp.nutricion', [
			'ngResource',
			'ui.router',
			'toaconsejosApp.nutricion.main',
			'toaconsejosApp.nutricion.list',
			'toaconsejosApp.nutricion.create'
		])
		.config(configNutricionRoutes);

	// inject configNutricionRoutes dependencies
	configNutricionRoutes.$inject = ['$urlRouterProvider', '$stateProvider'];

	/**
	 * Route configuration function configuring the passed $stateProvider.
	 * Register the abstract nutricion state with the nutricion template
	 * paired with the NutricionController as 'index'.
	 * The injectable 'nutricions' is resolved as a list of all nutricions
	 * and can be injected in all sub controllers.
	 *
	 * @param {$stateProvider} $stateProvider - The state provider to configure
	 */
	function configNutricionRoutes($urlRouterProvider, $stateProvider) {
		// The nutricion state configuration
		var nutricionState = {
			name: 'nutricion',
			url: '/consejos/nutricion',
			abstract: true,
			templateUrl: 'app/consejos/nutricion/nutricion.html',
			controller: 'NutricionController',
			controllerAs: 'index'
		};

		$urlRouterProvider.when('/nutricion', '/nutricion/');
		$stateProvider.state(nutricionState);
	}

})();
