'use strict';

describe('Controller: NutricionCtrl', function () {

	// load the controller's module
	beforeEach(module('toaconsejosApp.nutricion'));

	var controller;
	var scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function ($controller, $rootScope) {
		scope = $rootScope.$new();
		controller = $controller('NutricionCtrl', {
			// $scope: scope
		});
	}));

	it('object should exist', function () {
		Should.exist(controller);
		controller.should.be.an.instanceof(Object);
	});

});
