(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.nutricion.main module
	 * and configure it.
	 *
	 * @requires ui.router
	 * @requires toaconsejosApp.mainMenu
	 */

	angular
		.module('toaconsejosApp.nutricion.main', [
			'ui.router',
			'toaconsejosApp.mainMenu'
		])
		.config(configNutricionMainRoutes);

	// inject configNutricionMainRoutes dependencies
	configNutricionMainRoutes.$inject = ['$stateProvider', 'mainMenuProvider'];

	/**
	 * Route configuration function configuring the passed $stateProvider.
	 * Register the nutricion.main state with the list template for the
	 * 'main' view paired with the NutricionMainController as 'main'.
	 *
	 * @param {$stateProvider} $stateProvider - The state provider to configure
	 * @param {mainMenuProvider} mainMenuProvider - The service to pass navigation information to
	 */
	function configNutricionMainRoutes($stateProvider, mainMenuProvider) {
		// The main state configuration
		var mainState = {
			name: 'nutricion.main',
			parent: 'nutricion',
			url: '/',
			authenticate: true,
			role: 'user',
			views: {
				'@nutricion': {
					templateUrl: 'app/consejos/nutricion/main/main.html',
					controller: 'NutricionMainController',
					controllerAs: 'main'
				}
			}
		};

		$stateProvider.state(mainState);

		mainMenuProvider.addMenuItem({
			name: 'Nutricion',
			state: mainState.name,
			role: 'user'
		});
	}

})();
