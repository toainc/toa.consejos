(function () {
	'use strict';

	/**
	 * Register the list controller as NutricionMainController
	 */

	angular
		.module('toaconsejosApp.nutricion.main')
		.controller('NutricionMainController', NutricionMainController);

	// add NutricionMainController dependencies to inject
	NutricionMainController.$inject = ['$state'];

	/**
	 * NutricionMainController constructor
	 */
	function NutricionMainController($state) {
		var vm = this;
		// switch to the list state
		vm.showList = showList;

		/**
		 * Activate the nutricion.list state
		 */
		function showList() {
			$state.go('nutricion.list');
		}
	}

})();
