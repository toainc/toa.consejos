/**
 * @ngdoc controller
 * @name toaconsejosApp.nutricion.create.controller:NutricionCreateController
 * @description
 * Controller of the nutricion create page of the admin section
 */

(function () {
	'use strict';

	/**
	 * Register the create controller as NutricionCreateController
	 */

	angular
		.module('toaconsejosApp.nutricion.create')
		.controller('NutricionCreateController', NutricionCreateController);

	/**
	 * @ngdoc function
	 * @name toaconsejosApp.nutricion.create.provider:NutricionCreateController
	 * @description
	 * Provider of the {@link toaconsejosApp.nutricion.create.controller:NutricionCreateController NutricionCreateController}
	 *
	 * @param {Service} Auth The Auth service to use
	 * @param {Service} $mdDialog The mdDialog service to use
	 * @param {Service} Nutricion The Nutricion resource
	 * @param {Service} NutricionService The Nutricion service to use
	 * @param {Service} Toast The Toast service to use
	 * @returns {Service} {@link toaconsejosApp.nutricion.create.controller:NutricionCreateController NutricionCreateController}
	 */

	NutricionCreateController.$inject = ['$mdDialog', 'Nutricion', 'NutricionService', 'Toast'];

	function NutricionCreateController($mdDialog, Nutricion, NutricionService, Toast) {
		var vm = this;

		/**
		 * @ngdoc property
		 * @name nutricion
		 * @propertyOf toaconsejosApp.nutricion.create.controller:NutricionCreateController
		 * @description
		 * The new nutricion data
		 *
		 * @returns {Object} The nutricion data
		 */
		vm.nutricion = new Nutricion();

		// view model bindings (documented below)
		vm.create = createNutricion;
		vm.close = hideDialog;
		vm.cancel = cancelDialog;

		/**
		 * @ngdoc function
		 * @name createNutricion
		 * @methodOf toaconsejosApp.nutricion.create.controller:NutricionCreateController
		 * @description
		 * Create a new nutricion by using the NutricionService create method
		 *
		 * @param {form} [form] The form to gather the information from
		 */
		function createNutricion(form) {
			// refuse to work with invalid data
			if (vm.nutricion._id || (form && !form.$valid)) {
				return;
			}

			NutricionService.create(vm.nutricion)
				.then(createNutricionSuccess)
				.catch(createNutricionCatch);

			function createNutricionSuccess(newNutricion) {
				Toast.show({
					type: 'success',
					text: 'Nutricion ' + newNutricion.name + ' has been created',
					link: {state: 'nutricion.list.detail', params: {id: newNutricion._id}}
				});
				vm.close();
			}

			function createNutricionCatch(err) {
				if (form && err) {
					form.setResponseErrors(err);
				}

				Toast.show({
					type: 'warn',
					text: 'Error while creating a new Nutricion'
				});
			}
		}

		/**
		 * @ngdoc function
		 * @name hide
		 * @methodOf toaconsejosApp.nutricion.create.controller:NutricionCreateController
		 * @description
		 * Hide the dialog
		 */
		function hideDialog() {
			$mdDialog.hide();
		}

		/**
		 * @ngdoc function
		 * @name cancel
		 * @methodOf toaconsejosApp.nutricion.create.controller:NutricionCreateController
		 * @description
		 * Cancel the dialog
		 */
		function cancelDialog() {
			$mdDialog.cancel();
		}
	}
})();
