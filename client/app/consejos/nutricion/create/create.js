(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.nutricion.create module
	 * and configure it.
	 *
	 * @requires ui.router
	 * @requires ngMessages
	 * @requires ngMaterial
	 * @requires {toaconsejosApp.mongooseError}
	 * @requires {toaconsejosApp.remoteUnique}
	 * @requires {toaconsejosApp.nutricion.service}
	 */

	angular
		.module('toaconsejosApp.nutricion.create', [
			'ui.router',
			'ngMessages',
			'ngMaterial',
			'toaconsejosApp.mongooseError',
			'toaconsejosApp.remoteUnique',
			'toaconsejosApp.nutricion.service'
		])
		.config(configureNutricionCreateRoutes);

	// inject configNutricion.CreateRoutes dependencies
	configureNutricionCreateRoutes.$inject = ['$stateProvider'];

	/**
	 * Route configuration function configuring the passed $stateProvider.
	 * Register the 'nutricion.list.create' state. The onEnterNutricionListCreateView
	 * function will be called when entering the state and open a modal dialog
	 * with the app/consejos/nutricion/create/create.html template loaded.
	 *
	 * @param {$stateProvider} $stateProvider - The state provider to configure
	 */
	function configureNutricionCreateRoutes($stateProvider) {
		var  createListState = {
			name: 'nutricion.list.create',
			parent: 'nutricion.list',
			url: '/create',
			authenticate: true,
			role: 'user',
			onEnter: onEnterNutricionListCreateView
		};

		$stateProvider.state(createListState);
	}

	/**
	 * Function that executes when entering the nutricion.list.create state.
	 * Open the create dialog
	 */

	onEnterNutricionListCreateView.$inject = ['$rootScope', '$state', '$mdDialog'];

	function onEnterNutricionListCreateView($rootScope, $state, $mdDialog) {
		var unregisterListener = $rootScope.$on('$stateChangeStart', onStateChange);

		$mdDialog.show({
			controller: 'NutricionCreateController',
			controllerAs: 'create',
			templateUrl: 'app/consejos/nutricion/create/create.html',
			clickOutsideToClose: false
		}).then(transitionTo, transitionTo);

		/**
		 * Function executed when resolving or rejecting the
		 * dialog promise.
		 *
		 * @param {*} answer - The result of the dialog callback
		 * @returns {promise}
		 */
		function transitionTo(answer) {
			return $state.transitionTo('nutricion.list');
		}

		/**
		 * Function executed when changing the state.
		 * Closes the create dialog
		 */
		function onStateChange() {
			unregisterListener();
			//$mdDialog.hide();
		}
	}

})();
