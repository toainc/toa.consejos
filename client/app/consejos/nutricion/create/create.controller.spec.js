'use strict';

describe('Controller: NutricionCreateController', function () {

	// load the controller's module
	beforeEach(module('toaconsejosApp.nutricion.create'));

	var controller;
	var scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function ($controller, $rootScope) {
		scope = $rootScope.$new();
		controller = $controller('NutricionCreateController', {
			// $scope: scope
		});
	}));

	it('object should exist', function () {
		Should.exist(controller);
		controller.should.be.an.instanceof(Object);
	});
});
