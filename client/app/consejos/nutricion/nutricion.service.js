(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.nutricion.service module.
	 * Register the nutricion resource as Nutricion, register the
	 * service as NutricionService.
	 *
	 * @requires {toaconsejosApp.resource}
	 */
	angular
		.module('toaconsejosApp.nutricion.service', ['toaconsejosApp.resource'])
		.factory('Nutricion', Nutricion)
		.service('NutricionService', NutricionService);

	// add Nutricion dependencies to inject
	Nutricion.$inject = ['Resource'];

	/**
	 * Nutricion resource constructor
	 */
	function Nutricion($resource) {
		// factory members
		var apiURL = '/api/consejos/nutricion';
		// public API
		return $resource(apiURL + '/:id/:controller');
	}

	// add NutricionService dependencies to inject
	NutricionService.$inject = ['Nutricion'];

	/**
	 * NutricionService constructor
	 * AngularJS will instantiate a singleton by calling "new" on this function
	 *
	 * @param {$resource} Nutricion The resource provided by toaconsejosApp.nutricion.resource
	 * @returns {Object} The service definition for the NutricionService service
	 */
	function NutricionService(Nutricion) {

		return {
			create: create,
			update: update,
			remove: remove
		};

		/**
		 * Save a new nutricion
		 *
		 * @param  {Object}   nutricion - nutricionData
		 * @param  {Function} callback - optional
		 * @return {Promise}
		 */
		function create(nutricion, callback) {
			var cb = callback || angular.noop;

			return Nutricion.create(nutricion,
				function (nutricion) {
					return cb(nutricion);
				},
				function (err) {
					return cb(err);
				}).$promise;
		}

		/**
		 * Remove a nutricion
		 *
		 * @param  {Object}   nutricion - nutricionData
		 * @param  {Function} callback - optional
		 * @return {Promise}
		 */
		function remove(nutricion, callback) {
			var cb = callback || angular.noop;

			return Nutricion.remove({id: nutricion._id},
				function (nutricion) {
					return cb(nutricion);
				},
				function (err) {
					return cb(err);
				}).$promise;
		}

		/**
		 * Create a new nutricion
		 *
		 * @param  {Object}   nutricion - nutricionData
		 * @param  {Function} callback - optional
		 * @return {Promise}
		 */
		function update(nutricion, callback) {
			var cb = callback || angular.noop;

			return Nutricion.update(nutricion,
				function (nutricion) {
					return cb(nutricion);
				},
				function (err) {
					return cb(err);
				}).$promise;
		}
	};
})();
