(function () {
	'use strict';

	// register the controller as NutricionController
	angular
		.module('toaconsejosApp.nutricion')
		.controller('NutricionController', NutricionController);

	// add NutricionController dependencies to inject
	// NutricionController.$inject = [];

	/**
	 * NutricionController constructor. Main controller for the toaconsejosApp.nutricion
	 * module.
	 *
	 * @param {$scope} $scope - The scope to listen for events
	 * @param {socket.io} socket - The socket to register updates
	 */
	function NutricionController() {
		// var vm = this;
	}

})();
