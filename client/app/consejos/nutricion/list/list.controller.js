(function () {
	'use strict';

	/**
	 * Register the list controller as NutricionListController
	 */
	angular
		.module('toaconsejosApp.nutricion.list')
		.controller('NutricionListController', NutricionListController);

	// add NutricionListController dependencies to inject
	NutricionListController.$inject = ['$scope', 'socket', '$state', 'nutricions', 'ToggleComponent'];

	/**
	 * NutricionListController constructor
	 *
	 * @param {Object} $scope - The current scope
	 * @param {Object} socket - The socket service to register to
	 * @param {$state} $state - The $state to activate routing states on
	 * @param {Array} nutricions - The list of nutricions resolved for this route
	 * @param {Service} ToggleComponent - The service for switching the detail view
	 */
	function NutricionListController($scope, socket, $state, nutricions, ToggleComponent) {
		var vm = this;

		// the array of nutricions
		vm.nutricions = nutricions;
		// toggle detail view
		vm.toggleDetails = toggleDetails;

		// initialize the controller
		activate();

		/**
		 * Register socket updates and unsync on scope $destroy event
		 */
		function activate() {
			socket.syncUpdates('nutricion', vm.nutricions);
			$scope.$on('$destroy', unsyncNutricionUpdates);

			function unsyncNutricionUpdates() {
				socket.unsyncUpdates('nutricion');
			}
		}

		/**
		 * Toggle the detail view
		 */
		function toggleDetails() {
			ToggleComponent('nutricion.detailView').toggle();
		}
	}

})();
