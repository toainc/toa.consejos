(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.nutricion.list.items module
	 * @requires ui.router
	 */

	angular
		.module('toaconsejosApp.nutricion.list.items', ['ui.router']);

})();
