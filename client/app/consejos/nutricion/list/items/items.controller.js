(function () {
	'use strict';

	/**
	 * Register the list controller as NutricionItemsController
	 */

	angular
		.module('toaconsejosApp.nutricion.list.items')
		.controller('NutricionItemsController', NutricionItemsController);

	// add NutricionItemsController dependencies to inject
	NutricionItemsController.$inject = ['$state'];

	/**
	 * NutricionItemsController constructor
	 */
	function NutricionItemsController($state) {
		var vm = this;

		// the selected item id
		var curNutricionId = null;

		// check if this item is selected
		vm.isSelected = isSelected;
		// switch to the detail state
		vm.showInDetails = showInDetails;

		/**
		 * Check if the passed item is the current selected item
		 *
		 * @param {Object} nutricion - The object to check for selection
		 */
		function isSelected(nutricion) {
			return curNutricionId === nutricion._id;
		}

		/**
		 * Open the detail state with the selected item
		 *
		 * @param {Object} nutricion - The nutricion to edit
		 */
		function showInDetails(nutricion) {
			curNutricionId = nutricion._id;
			$state.go('nutricion.list.detail', {'id': curNutricionId});
		}
	}

})();
