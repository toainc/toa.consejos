(function () {
	'use strict';

	/**
	 * Register the edit controller as NutricionDetailController
 	 */

	angular
		.module('toaconsejosApp.nutricion.list.detail')
		.controller('NutricionDetailController', NutricionDetailController);

	// add NutricionDetailController dependencies to inject
	NutricionDetailController.$inject = ['$state', 'nutricion'];

	/**
	 * NutricionDetailController constructor
	 */
	function NutricionDetailController($state, nutricion) {
		var vm = this;

		// the current nutricion to display
		vm.nutricion = nutricion;
		// switch to the edit state
		vm.edit = edit;
		// switch to the parent state
		vm.goBack = goBack

		/**
		 * Open the edit state with the current nutricion
		 *
		 */
		function edit() {
			$state.go('^.edit', {'id': vm.nutricion._id});
		}

		/**
		 * Return to the parent state
		 *
		 */
		function goBack() {
			$state.go('^');
		}
	}
})();
