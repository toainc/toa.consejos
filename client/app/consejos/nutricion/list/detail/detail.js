(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.nutricion.list.detail submodule
	 * and configure it.
	 *
   * @requires ui.router
	 * @requires angularMoment
	 */

	angular
		.module('toaconsejosApp.nutricion.list.detail', [
			'ui.router',
			'angularMoment'
		])
		.config(configureNutricionListDetail);

	// inject configNutricionRoutes dependencies
	configureNutricionListDetail.$inject = ['$stateProvider'];

	/**
	 * Route configuration function configuring the passed $stateProvider.
	 * Register the 'nutricion.detail' state with the detail template
	 * paired with the NutricionDetailController as 'detail' for the
	 * 'sidenav' sub view.
	 * 'nutricion' is resolved as the nutricion with the id found in
	 * the state parameters.
	 *
	 * @param {$stateProvider} $stateProvider - The state provider to configure
	 */
	function configureNutricionListDetail($stateProvider) {
		// The detail state configuration
		var detailState = {
			name: 'nutricion.list.detail',
			parent: 'nutricion.list',
			url: '/:id',
			authenticate: true,
			role: 'user',
			onEnter: onEnterNutricionListDetail,
			views: {
				'detail@nutricion.list': {
					templateUrl: 'app/consejos/nutricion/list/detail/detail.html',
					controller: 'NutricionDetailController',
					controllerAs: 'detail',
					resolve: {nutricion: resolveNutricionFromArray}
				}
			}
		};

		$stateProvider.state(detailState);
	}

	// inject onNutricionListDetailEnter dependencies
	onEnterNutricionListDetail.$inject = ['$timeout', 'ToggleComponent'];

	/**
	 * Executed when entering the nutricion.list.detail state. Open the component
	 * registered with the component id 'nutricion.detailView'.
	 *
 	 * @params {$timeout} $timeout - The $timeout service to wait for view initialization
	 * @params {ToggleComponent} ToggleComponent - The service to toggle the detail view
	 */
	function onEnterNutricionListDetail($timeout, ToggleComponent) {
		$timeout(showDetails, 0, false);

		function showDetails() {
			ToggleComponent('nutricion.detailView').open();
		}
	}

	// inject resolveNutricionFromArray dependencies
	resolveNutricionFromArray.$inject = ['nutricions', '$stateParams', '_'];

	/**
	 * Resolve dependencies for the nutricion.detail state
	 *
	 * @params {Array} nutricions - The array of nutricions
	 * @params {Object} $stateParams - The $stateParams to read the nutricion id from
	 * @returns {Object|null} The nutricion whose value of the _id property equals $stateParams._id
	 */
	function resolveNutricionFromArray(nutricions, $stateParams, _) {
		return _.find(nutricions, {'_id': $stateParams.id});
	}

})();
