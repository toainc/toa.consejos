(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.nutricion.list.edit module
	 * and configure it.
	 *
	 * @requires 'ui.router',
	 * @requires 'ngMaterial',
	 * @requires toaconsejosApp.mongooseError
	 * @requires toaconsejosApp.nutricion.service
	 */

	angular
		.module('toaconsejosApp.nutricion.list.edit', [
			'ui.router',
			'ngMaterial',
			'toaconsejosApp.mongooseError',
			'toaconsejosApp.nutricion.service'
		])
		.config(configureNutricionListEdit);

	// inject configNutricionListEdit dependencies
	configureNutricionListEdit.$inject = ['$stateProvider'];

	/**
	 * Route configuration function configuring the passed $stateProvider.
	 * Register the nutricion.list.edit state with the edit template
	 * paired with the NutricionEditController as 'edit' for the
	 * 'detail@nutricion.list' view.
	 * 'nutricion' is resolved as the nutricion with the id found in
	 * the state parameters.
	 *
	 * @param {$stateProvider} $stateProvider - The state provider to configure
	 */
	function configureNutricionListEdit($stateProvider) {
		// The edit state configuration.
		var editState = {
			name: 'nutricion.list.edit',
			parent: 'nutricion.list',
			url: '/edit/:id',
			authenticate: true,
			role: 'user',
			onEnter: onEnterNutricionListEdit,
			views: {
				'detail@nutricion.list': {
					templateUrl: 'app/consejos/nutricion/list/edit/edit.html',
					controller: 'NutricionEditController',
					controllerAs: 'edit',
					resolve: {nutricion: resolveNutricionFromArray}
				}
			}
		};

		$stateProvider.state(editState);
	}

	// inject onNutricionListEditEnter dependencies
	onEnterNutricionListEdit.$inject = ['$timeout', 'ToggleComponent'];

	/**
	 * Executed when entering the nutricion.list.detail state. Open the component
	 * registered with the component id 'nutricion.detailView'.
	 *
	 * @params {$timeout} $timeout - The $timeout service to wait for view initialization
	 * @params {ToggleComponent} ToggleComponent - The service to toggle the detail view
	 */
	function onEnterNutricionListEdit($timeout, ToggleComponent) {
		$timeout(showDetails, 0, false);

		function showDetails() {
			ToggleComponent('nutricion.detailView').open();
		}
	}

	// inject resolveNutricionDetailRoute dependencies
	resolveNutricionFromArray.$inject = ['nutricions', '$stateParams', '_'];

	/**
	 * Resolve dependencies for the nutricion.list.edit state. Get the nutricion
	 * from the injected Array of nutricions by using the '_id' property.
	 *
	 * @params {Array} nutricions - The array of nutricions
	 * @params {Object} $stateParams - The $stateParams to read the nutricion id from
	 * @params {Object} _ - The lodash service to find the requested nutricion
	 * @returns {Object|null} The nutricion whose value of the _id property equals $stateParams._id
	 */
	function resolveNutricionFromArray(nutricions, $stateParams, _) {
		//	return Nutricion.get({id: $stateParams.id}).$promise;
		return _.find(nutricions, {'_id': $stateParams.id});
	}

})();
