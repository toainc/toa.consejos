/**
 * @ngdoc controller
 * @name toaconsejosAppnutricion.list.edit.controller:NutricionEditController
 * @description
 * Controller of the nutricion edit page of the admin section
 */

(function () {
	'use strict';

	/**
	 * Register the edit controller as NutricionEditController
	 */

	angular
		.module('toaconsejosApp.nutricion.list.edit')
		.controller('NutricionEditController', NutricionEditController);

	/**
	 * @ngdoc function
	 * @name toaconsejosAppnutricion.list.edit.provider:NutricionEditController
	 * @description
	 * Provider of the {@link toaconsejosAppnutricion.list.edit.controller:NutricionEditController NutricionEditController}
	 * @param {Service} $state The state service to use
	 * @param {Service} $stateParams The stateParams service to use
	 * @param {Service} $mdDialog The dialog service to use
	 * @param {Service} Toast The Toast service to use
	 * @param {Service} NutricionService The NutricionService to use
	 * @param {Resource} nutricion The nutricion data to use
	 */

	NutricionEditController.$inject = ['$state', '$stateParams', '$mdDialog', 'Toast', 'NutricionService', 'nutricion'];

	function NutricionEditController($state, $stateParams, $mdDialog, Toast, NutricionService, nutricion) {
		var vm = this;

		// defaults
		vm.nutricion = angular.copy(nutricion, vm.nutricion);
		vm.displayName = nutricion.name;

		// view model bindings
		vm.update = update;
		vm.remove = remove;
		vm.goBack = goBack;
		vm.showList = showList;

		/**
		 * Open the detail state with the current nutricion
		 *
		 */
		function goBack() {
			$state.go('^.detail', {id: vm.nutricion._id});
		}

		/**
		 * Open the nutricion list state
		 *
		 */
		function showList() {
			$state.go('^');
		}
		/**
		 * Updates a nutricion by using the NutricionService save method
		 * @param {Form} [form]
		 */
		function update(form) {
			// refuse to work with invalid data
			if (!vm.nutricion._id || form && !form.$valid) {
				return;
			}

			NutricionService.update(vm.nutricion)
				.then(updateNutricionSuccess)
				.catch(updateNutricionCatch);

			function updateNutricionSuccess(updatedNutricion) {
				// update the display name after successful save
				vm.displayName = updatedNutricion.name;
				Toast.show({text: 'Nutricion ' + vm.displayName + ' updated'});
				if (form) {
					form.$setPristine();
				}
			}

			function updateNutricionCatch(err) {
				Toast.show({
					type: 'warn',
					text: 'Error while updating Nutricion ' + vm.displayName,
					link: {state: $state.$current, params: $stateParams}
				});

				if (form && err) {
					form.setResponseErrors(err.data);
				}
			}
		}

		/**
		 * Show a dialog to ask the nutricion if she wants to delete the current selected nutricion.
		 * @param {AngularForm} form - The form to pass to the remove handler
		 * @param {$event} ev - The event to pass to the dialog service
		 */
		function remove(form, ev) {
			var confirm = $mdDialog.confirm()
				.title('Delete nutricion ' + vm.displayName + '?')
				.content('Do you really want to delete nutricion ' + vm.displayName + '?')
				.ariaLabel('Delete nutricion')
				.ok('Delete nutricion')
				.cancel('Cancel')
				.targetEvent(ev);

			$mdDialog.show(confirm)
				.then(performRemove);

			/**
			 * Removes a nutricion by using the NutricionService remove method
			 * @api private
			 */
			function performRemove() {
				NutricionService.remove(vm.nutricion)
					.then(deleteNutricionSuccess)
					.catch(deleteNutricionCatch);

				function deleteNutricionSuccess() {
					Toast.show({type: 'success', text: 'Nutricion ' + vm.displayName + ' deleted'});
					vm.showList();
				}

				function deleteNutricionCatch(err) {
					Toast.show({
						type: 'warn',
						text: 'Error while deleting nutricion ' + vm.displayName,
						link: {state: $state.$current, params: $stateParams}
					});

					if (form && err) {
						form.setResponseErrors(err, vm.errors);
					}
				}
			}
		}
	}
})();
