'use strict';

describe('Controller: NutricionEditController', function () {

	// load the controller's module
	beforeEach(module('toaconsejosApp.nutricion.edit'));

	var controller;
	var scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function ($controller, $rootScope) {
		scope = $rootScope.$new();
		controller = $controller('NutricionEditController', {
			// $scope: scope
		});
	}));

	it('object should exist', function () {
		Should.exist(controller);
		controller.should.be.an.instanceof(Object);
	});
});
