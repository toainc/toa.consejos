(function () {
	'use strict';

	/**
	 * Introduce the toaconsejosApp.nutricion.list module
	 * and configure it.
	 * @requires ui.router
	 * @requires ngMaterial
	 * @requires toaconsejosApp.socket
	 * @requires toaconsejosApp.mainMenu,
	 * @requires toaconsejosApp.toggleComponent,
	 * @requires toaconsejosApp.nutricion.list.detail
	 * @requires toaconsejosApp.nutricion.list.edit
	 * @requires toaconsejosApp.nutricion.list.items
	 */

	angular
		.module('toaconsejosApp.nutricion.list', [
			'ngMaterial',
			'ui.router',
			'toaconsejosApp.socket',
			'toaconsejosApp.mainMenu',
			'toaconsejosApp.toggleComponent',
			'toaconsejosApp.nutricion.list.detail',
			'toaconsejosApp.nutricion.list.edit',
			'toaconsejosApp.nutricion.list.items'
		])
		.config(configNutricionListRoutes);

	// inject configNutricionListRoutes dependencies
	configNutricionListRoutes.$inject = ['$stateProvider', 'mainMenuProvider'];

	/**
	 * Route configuration function configuring the passed $stateProvider.
	 * Register the nutricion.list state with the list template fpr the
	 * 'main' view paired with the NutricionListController as 'list'.
	 *
	 * @param {$stateProvider} $stateProvider - The state provider to configure
	 */
	function configNutricionListRoutes($stateProvider, mainMenuProvider) {
		// The list state configuration
		var listState = {
			name: 'nutricion.list',
			parent: 'nutricion',
			url: '/list',
			authenticate: true,
			role: 'user',
			resolve: {
				nutricions:  resolveNutricions
			},
			views: {

				// target the unnamed view in the nutricion state
				'@nutricion': {
					templateUrl: 'app/consejos/nutricion/list/list.html',
					controller: 'NutricionListController',
					controllerAs: 'list'
				},

				// target the content view in the nutricion.list state
				'content@nutricion.list': {
					templateUrl: 'app/consejos/nutricion/list/items/items.html',
					controller: 'NutricionItemsController',
					controllerAs: 'items'
				}
			}
		};

		$stateProvider.state(listState);

		mainMenuProvider.addSubMenuItem('nutricion.main', {
			name: 'Nutricion List',
			state: listState.name
		});
	}

	// inject resolveNutricions dependencies
	resolveNutricions.$inject = ['Nutricion'];

	/**
	 * Resolve dependencies for the nutricion.list state
	 *
	 * @params {Nutricion} Nutricion - The service to query nutricions
	 * @returns {Promise} A promise that, when fullfilled, returns an array of nutricions
	 */
	function resolveNutricions(Nutricion) {
		return Nutricion.query().$promise;
	}

})();
