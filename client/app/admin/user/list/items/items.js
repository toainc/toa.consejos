	/**
	 * @ngdoc overview
	 * @name toaconsejosApp.admin.user.list.items
	 * @requires ui.router
	 * @requires components/listImage
	 *
	 * @description
	 * The `toaconsejosApp.admin.user.list.items` module which provides:
	 *
	 * - {@link toaconsejosApp.admin.user.list.items.controller:UserItemsController UserItemsController}
	 */

(function () {
	'use strict';

	angular
		.module('toaconsejosApp.admin.user.list.items', [
			'ui.router',
			'toaconsejosApp.listImage'
		]);

})();
