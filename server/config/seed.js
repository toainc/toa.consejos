/*
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var mongoose = require('mongoose');
var env = process.env.NODE_ENV || 'development';

var User = require('../api/user/user.model').model;


/*
 * Insert dummy data to test the application
 */
exports.users = [{
	provider: 'local',
	role: 'admin',
	name: 'Admin',
	password: 'password',
	active: true
}, {
	provider: 'local',
	role: 'root',
	name: 'Root',
	password: 'password',
	active: true
}, {
	provider: 'local',
	role: 'user',
	name: 'TOA',
	password: 'toa',
	active: true
}];

// Insert some data needed to bootstrap or init the application

if ('production' === env) {
	User.find({}).remove(function () {
		User.create(exports.users, function (err) {
			if (err) {
				console.error('Error while populating users: %s', err);
			} else {
				console.log('finished populating users');
			}
		});
	});
}

if ('development' === env) {
	console.log('Populating test and development data ...');

	User.find({}).remove(function () {
		User.create(exports.users, function (err) {
			if (err) {
				console.error('Error while populating users: %s', err);
			} else {
				console.log('finished populating users');
			}
		});
	});
}
