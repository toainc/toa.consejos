'use strict';

process.env.DATABASE_NAME = process.env.DATABASE_NAME || 'toaconsejos';

module.exports = {

	ip: process.env.ip || undefined,

	port: process.env.PORT || 80,

	mongo: {
		uri: 'mongodb://40.78.108.151/' + process.env.DATABASE_NAME
	},

	seedDB: true
};
