/**
 * Module for the controller definition of the nutricion api.
 * The NutricionController is handling /api/consejos/nutricion requests.
 * @module {nutricion:controller~NutricionController} nutricion:controller
 * @requires {@link ParamController}
 */
'use strict';

module.exports = NutricionController;

var ParamController = require('../../lib/controllers/param.controller');

/**
 * The Nutricion model instance
 * @type {nutricion:model~Nutricion}
 */
var Nutricion = require('./nutricion.model').model;

/**
 * NutricionController constructor
 * @classdesc Controller that handles /api/consejos/nutricion route requests
 * for the nutricion api.
 * Uses the 'nutricionId' parameter and the 'nutricionParam' request property
 * to operate with the [main nutricion API Model]{@link nutricion:model~Nutricion} model.
 * @constructor
 * @inherits ParamController
 * @see nutricion:model~Nutricion
 */
function NutricionController(router) {
	ParamController.call(this, Nutricion,  router);

	// modify select only properties
	// this.select = ['-__v'];

	// omit properties on update
	// this.omit = ['hashedPassword'];

	// property to return (maybe a virtual getter of the model)
	// this.defaultReturn = 'profile';
}

// define properties for the NutricionController here
NutricionController.prototype = {

	/**
	 * Set our own constructor property for instanceof checks
	 * @private
	 */
	constructor: NutricionController

};

// inherit from ParamController
NutricionController.prototype = Object.create(ParamController.prototype);

