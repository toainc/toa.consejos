/**
 * Module for registering broadcast updates to clients when
 * the Nutricion model changes. Exports the
 * [register function]{@link nutricion:socket~registerNutricionSockets}
 * to register the model schema events on the socket instance.
 * @module {function} nutricion:socket
 * @requires {@link nutricion:model}
 */
'use strict';

/**
 * The Nutricion model instance
 * @type {nutricion:model~Nutricion}
 */
var Nutricion = require('./nutricion.model').model;

// export the function to register all socket broadcasts
exports.register = registerNutricionSockets;

/**
 * Register Nutricion model change events on the passed socket
 * @param {socket.io} socket - The socket object to register the Nutricion model events on
 */
function registerNutricionSockets(socket) {
	Nutricion.schema.post('save', function (doc) {
		onSave(socket, doc);
	});

	Nutricion.schema.post('remove', function (doc) {
		onRemove(socket, doc);
	});
}

/**
 * Emit a Nutricion save event on a socket object: 'nutricion:save'
 * @param {socket.io} socket - The socket object to emit the Nutricion save event on
 * @param {MogooseDocument} doc - The saved document that triggered the event
 * @param {function} cb - The callback function
 */
function onSave(socket, doc, cb) {
	socket.emit('nutricion:save', doc);
}

/**
 * Emit a Nutricion remove event on a socket object: 'nutricion:remove'
 * @param {socket.io} socket - The socket object to emit the Nutricion remove event on
 * @param {MogooseDocument} doc - The removed document that triggered the event
 * @param {function} cb - The callback function
 */
function onRemove(socket, doc, cb) {
	socket.emit('nutricion:remove', doc);
}
