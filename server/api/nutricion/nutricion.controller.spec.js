/* jshint unused:false */
'use strict';

var should = require('should');
var app = require('../../app');
var request = require('supertest');
var nutricionModel = require('./nutricion.model');

// Clear all nutricions
function cleanup(done) {
	nutricionModel.model.remove().exec().then(function () { done();	});
}

describe('/api/consejos/nutricion', function () {

	var nutricion;

	// reset nutricion before each test
	beforeEach(function () {
		nutricion = {
			name: 'Dog',
			info: 'Hello, this is dog.',
			active: true
		};
	});

	// Clear nutricions before each test
	beforeEach(cleanup);

	// Clear nutricions after each test
	afterEach(cleanup);

	describe('GET', function () {

		it('should respond with JSON array', function (done) {
			request(app)
				.get('/api/consejos/nutricion')
				.set('Accept', 'application/json')
				.expect(200)
				.expect('Content-Type', /json/)
				.end(function (err, res) {
					if (err) {
						return done(err);
					}
					res.body.should.be.instanceof(Array);
					done();
				});
		});

		it('should respond with an error for a malformed nutricion id parameter', function (done) {
			request(app)
				.get('/api/consejos/nutricion/malformedid')
				.set('Accept', 'application/json')
				.expect(400)
				.expect('Content-Type', /json/)
				.end(done);
		});

		it('should respond with an not found error for a not existing nutricion id', function (done) {
			request(app)
				.get('/api/consejos/nutricion/cccccccccccccccccccccccc')
				.set('Accept', 'application/json')
				.expect(404)
				.expect('Content-Type', /json/)
				.end(done);
		});

		it('should return a nutricion for its id', function (done) {
			nutricionModel.model(nutricion).save(function (err, doc) {
				request(app)
					.get('/api/consejos/nutricion/' + doc._id)
					.set('Accept', 'application/json')
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function (err, res) {
						if (err) {
							return done(err);
						}
						res.body.should.be.an.Object.and.have.properties(nutricion);
						res.body._id.should.exist;
						done();
					});
			});
		});

	});

	describe('POST', function () {

		it('should create a new nutricion and respond with 201 and the created nutricion', function (done) {
			request(app)
				.post('/api/consejos/nutricion')
				.set('Accept', 'application/json')
				.send(nutricion)
				.expect(201)
				.expect('Content-Type', /json/)
				.end(function (err, res) {
					if (err) {
						return done(err);
					}
					res.body.should.be.an.Object.and.have.properties(nutricion);
					res.body._id.should.exist;
					done();
				});
		});

	});

	describe('PUT', function () {

		it('should return an error if attempting a put without an id', function (done) {
			request(app)
				.put('/api/consejos/nutricion')
				.set('Accept', 'application/json')
				.send(nutricion)
				.expect(404)
				.end(done);
		});

		it('should respond with an not found error for a not existing nutricion id', function (done) {
			request(app)
				.put('/api/consejos/nutricion/cccccccccccccccccccccccc')
				.set('Accept', 'application/json')
				.expect(404)
				.expect('Content-Type', /json/)
				.end(done);
		});

		it('should update a nutricion and respond with the updated nutricion', function (done) {
			request(app)
				.post('/api/consejos/nutricion')
				.set('Accept', 'application/json')
				.send(nutricion)
				.end(function (err, res) {
					if (err) {
						return done(err);
					}
					nutricion.name = 'Cat';
					// check if id is stripped on update
					nutricion._id = 'malformed id string';
					request(app)
						.put('/api/consejos/nutricion/' + res.body._id)
						.set('Accept', 'application/json')
						.send(nutricion)
						.expect(200)
						.expect('Content-Type', /json/)
						.end(function (err, res) {
							if (err) {
								return done(err);
							}
							res.body.should.be.an.Object.and.have.property('name', nutricion.name);
							done();
						});
				});
		});

	});

	describe('DELETE', function () {

		it('should return an error if attempting a delete without an id', function (done) {
			request(app)
				.delete('/api/consejos/nutricion')
				.set('Accept', 'application/json')
				.expect(404)
				.end(done);
		});

		it('should respond with an not found error for a not existing nutricion id', function (done) {
			request(app)
				.delete('/api/consejos/nutricion/cccccccccccccccccccccccc')
				.set('Accept', 'application/json')
				.expect(404)
				.expect('Content-Type', /json/)
				.end(done);
		});

		it('should delete a nutricion and respond with 204', function (done) {
			request(app)
				.post('/api/consejos/nutricion')
				.set('Accept', 'application/json')
				.send(nutricion)
				.end(function (err, res) {
					if (err) {
						return done(err);
					}
					request(app)
						.delete('/api/consejos/nutricion/' + res.body._id)
						.set('Accept', 'application/json')
						.expect(204)
						.end(done);
				});
		});
	});
});
