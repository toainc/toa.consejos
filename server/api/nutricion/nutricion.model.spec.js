/* jshint unused:false */
'use strict';

var should = require('should');

var nutricion = require('./nutricion.model');
var nutricionDefinition = nutricion.definition;
var nutricionSchema= nutricion.schema;
var Nutricion = nutricion.model;

var nutricionData = [
	{
		name: 'Dog',
		info: 'Hello, this is dog.',
		active: true
	}, {
		name: 'Bugs Bunny',
		info: 'Famous Bunny.',
		active: true
	}, {
		name: 'Nyan Cat',
		info: 'No comment.',
		active: false
	}
];

// Clear all nutricions
function cleanup(done) {
	Nutricion.remove().exec().then(function () { done();	});
}

describe('Nutricion Model', function () {

	// Clear nutricions before testing
	before(cleanup);

	// Clear nutricions after testing
	after(cleanup);

// Check test conditions for nutricion tests
	it('should start with no nutricions', function (done) {
		Nutricion.find({}, function (err, nutricions) {
			nutricions.should.have.length(0);
			done(err);
		});
	});

	describe('basic crud operations', function () {

		var nutricionModel = new Nutricion(nutricionData[0]);

		// Clear nutricions after running this suite
		after(cleanup);

		it('should insert a new nutricion', function (done) {
			nutricionModel.save(function (err, nutricion) {
				nutricion.should.have.properties(nutricionModel);
				done(err);
			});
		});

		it('should insert a list of nutricions', function (done) {
			Nutricion.create(nutricionData, function (err, nutricion) {
				// slice err argument
				Array.prototype.slice.call(arguments, 1)
					.should.have.lengthOf(nutricionData.length);
				done(err);
			});
		});


		it('should find a nutricion by _id property', function (done) {
			Nutricion.findById(nutricionModel._id, function (err, nutricion) {
				nutricion.should.have.properties(nutricionData[0]);
				done(err);
			});
		});

		it('should update a nutricion', function (done) {
			nutricionModel.name = 'foo';
			nutricionModel.save(function (err) { done(err);	});
		});

		it('should remove a nutricion', function (done) {
			nutricionModel.remove(function (err) { done(err); });
		});
	}); // crud
});
