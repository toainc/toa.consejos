/**
 * Module for handling nutricion requests.
 * Initializing the [NutricionController]{@link nutricion:controller~NutricionController}
 * and configuring the express router to handle the nutricion api
 * for /api/nutricions routes. All Routes are registered after the
 * [request parameters]{@link nutricion:parameters} have been
 * added to the router instance.
 * Exports the configured express router for the nutricion api routes
 * @module {express.Router} nutricion
 * @requires {@link module:middleware}
 * @requires {@link nutricion:controller~NutricionController}
 */
'use strict';

var router = require('express').Router();
var contextService = require('request-context');
var middleware = require('../../lib/middleware');
var NutricionController = require('./nutricion.controller');
var auth = require('../../lib/auth/auth.service');

// Export the configured express router for the nutricion api routes
module.exports = router;

/**
 * The api controller
 * @type {nutricion:controller~NutricionController}
 */
var controller = new NutricionController(router);

// register nutricion route parameters, uncomment if needed
// var registerNutricionParameters = require('./nutricion.params');
// registerNutricionParameters(router);

// add context for auth sensitive resources
var addRequestContext = contextService.middleware('request');

// add the authenticated user to the created acl context
var addUserContext = auth.addAuthContext('request:acl.user');

// check if the request is made by an authenticated user with at least the user role
var isAuthenticated = auth.hasRole('user');

// apply auth middleware to all routes
router.route('*').all(addRequestContext, isAuthenticated, addUserContext);

// register nutricion routes
router.route('/')
	.get(controller.index)
	.post(controller.create);

router.route('/' + controller.paramString)
	.get(controller.show)
	.delete(controller.destroy)
	.put(controller.update)
	.patch(controller.update);
