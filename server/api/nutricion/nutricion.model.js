/**
 * An module for defining and initializing the Nutricion model.
 * Exporting the Nutricion model definition, schema and model instance.
 * @module {Object} nutricion:model
 * @property {Object} definition - The [definition object]{@link nutricion:model~NutricionDefinition}
 * @property {MongooseSchema} schema - The [mongoose model schema]{@link nutricion:model~NutricionSchema}
 * @property {MongooseModel} model - The [mongoose model]{@link nutricion:model~Nutricion}
 */
'use strict';

var mongoose = require('mongoose');
var requestContext = require('mongoose-request-context');
var createdModifiedPlugin = require('mongoose-createdmodified').createdModifiedPlugin;

/**
 * The Nutricion model definition
 * @type {Object}
 * @property {String} name - The name of this nutricion
 * @property {String} info - Details about this nutricion
 * @property {Boolean} active - Flag indicating this nutricion is active
 */
var NutricionDefinition = {
	name: {type: String, required: true},
	info: String,
	active: Boolean
};

/**
 * The Nutricion model schema
 * @type {MongooseSchema}
 */
var NutricionSchema = new mongoose.Schema(NutricionDefinition);

/**
 * Attach security related plugins
 */
NutricionSchema.plugin(createdModifiedPlugin);

NutricionSchema.plugin(requestContext, {
	propertyName: 'modifiedBy',
	contextPath: 'request:acl.user.name'
});

/**
 * Validations
 */
NutricionSchema
	.path('name')
	.validate(validateUniqueName, 'The specified name is already in use.');

/**
 *  The registered mongoose model instance of the Nutricion model
 *  @type {Nutricion}
 */
var Nutricion = mongoose.model('Nutricion', NutricionSchema);

module.exports = {

	/**
	 * The Nutricion model definition object
	 * @type {Object}
	 * @see nutricion:NutricionModel~NutricionDefinition
	 */
	definition: NutricionDefinition,

	/**
	 * The Nutricion model schema
	 * @type {MongooseSchema}
	 * @see nutricion:model~NutricionSchema
	 */
	schema: NutricionSchema,

	/**
	 * The Nutricion model instance
	 * @type {nutricion:model~Nutricion}
	 */
	model: Nutricion

};

/**
 * Validate the uniqueness of the given name
 *
 * @api private
 * @param {String} value - The username to check for uniqueness
 * @param {Function} respond - The callback function
 */
function validateUniqueName(value, respond) {
	// jshint validthis: true
	var self = this;

	// check for uniqueness of user name
	this.constructor.findOne({name: value}, function (err, nutricion) {
		if (err) {
			throw err;
		}

		if (nutricion) {
			// the searched name is my name or a duplicate
			return respond(self.id === nutricion.id);
		}

		respond(true);
	});
}
