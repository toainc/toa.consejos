/* jshint unused:false */
'use strict';

var should = require('should');

var salud = require('./salud.model');
var saludDefinition = salud.definition;
var saludSchema= salud.schema;
var Salud = salud.model;

var saludData = [
	{
		name: 'Dog',
		info: 'Hello, this is dog.',
		active: true
	}, {
		name: 'Bugs Bunny',
		info: 'Famous Bunny.',
		active: true
	}, {
		name: 'Nyan Cat',
		info: 'No comment.',
		active: false
	}
];

// Clear all saluds
function cleanup(done) {
	Salud.remove().exec().then(function () { done();	});
}

describe('Salud Model', function () {

	// Clear saluds before testing
	before(cleanup);

	// Clear saluds after testing
	after(cleanup);

// Check test conditions for salud tests
	it('should start with no saluds', function (done) {
		Salud.find({}, function (err, saluds) {
			saluds.should.have.length(0);
			done(err);
		});
	});

	describe('basic crud operations', function () {

		var saludModel = new Salud(saludData[0]);

		// Clear saluds after running this suite
		after(cleanup);

		it('should insert a new salud', function (done) {
			saludModel.save(function (err, salud) {
				salud.should.have.properties(saludModel);
				done(err);
			});
		});

		it('should insert a list of saluds', function (done) {
			Salud.create(saludData, function (err, salud) {
				// slice err argument
				Array.prototype.slice.call(arguments, 1)
					.should.have.lengthOf(saludData.length);
				done(err);
			});
		});


		it('should find a salud by _id property', function (done) {
			Salud.findById(saludModel._id, function (err, salud) {
				salud.should.have.properties(saludData[0]);
				done(err);
			});
		});

		it('should update a salud', function (done) {
			saludModel.name = 'foo';
			saludModel.save(function (err) { done(err);	});
		});

		it('should remove a salud', function (done) {
			saludModel.remove(function (err) { done(err); });
		});
	}); // crud
});
