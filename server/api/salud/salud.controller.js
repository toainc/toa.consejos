/**
 * Module for the controller definition of the salud api.
 * The SaludController is handling /api/consejos/salud requests.
 * @module {salud:controller~SaludController} salud:controller
 * @requires {@link ParamController}
 */
'use strict';

module.exports = SaludController;

var ParamController = require('../../lib/controllers/param.controller');

/**
 * The Salud model instance
 * @type {salud:model~Salud}
 */
var Salud = require('./salud.model').model;

/**
 * SaludController constructor
 * @classdesc Controller that handles /api/consejos/salud route requests
 * for the salud api.
 * Uses the 'saludId' parameter and the 'saludParam' request property
 * to operate with the [main salud API Model]{@link salud:model~Salud} model.
 * @constructor
 * @inherits ParamController
 * @see salud:model~Salud
 */
function SaludController(router) {
	ParamController.call(this, Salud,  router);

	// modify select only properties
	// this.select = ['-__v'];

	// omit properties on update
	// this.omit = ['hashedPassword'];

	// property to return (maybe a virtual getter of the model)
	// this.defaultReturn = 'profile';
}

// define properties for the SaludController here
SaludController.prototype = {

	/**
	 * Set our own constructor property for instanceof checks
	 * @private
	 */
	constructor: SaludController

};

// inherit from ParamController
SaludController.prototype = Object.create(ParamController.prototype);

