/**
 * An module for defining and initializing the Salud model.
 * Exporting the Salud model definition, schema and model instance.
 * @module {Object} salud:model
 * @property {Object} definition - The [definition object]{@link salud:model~SaludDefinition}
 * @property {MongooseSchema} schema - The [mongoose model schema]{@link salud:model~SaludSchema}
 * @property {MongooseModel} model - The [mongoose model]{@link salud:model~Salud}
 */
'use strict';

var mongoose = require('mongoose');
var requestContext = require('mongoose-request-context');
var createdModifiedPlugin = require('mongoose-createdmodified').createdModifiedPlugin;

/**
 * The Salud model definition
 * @type {Object}
 * @property {String} name - The name of this salud
 * @property {String} info - Details about this salud
 * @property {Boolean} active - Flag indicating this salud is active
 */
var SaludDefinition = {
	name: {type: String, required: true},
	info: String,
	active: Boolean
};

/**
 * The Salud model schema
 * @type {MongooseSchema}
 */
var SaludSchema = new mongoose.Schema(SaludDefinition);

/**
 * Attach security related plugins
 */
SaludSchema.plugin(createdModifiedPlugin);

SaludSchema.plugin(requestContext, {
	propertyName: 'modifiedBy',
	contextPath: 'request:acl.user.name'
});

/**
 * Validations
 */
SaludSchema
	.path('name')
	.validate(validateUniqueName, 'The specified name is already in use.');

/**
 *  The registered mongoose model instance of the Salud model
 *  @type {Salud}
 */
var Salud = mongoose.model('Salud', SaludSchema);

module.exports = {

	/**
	 * The Salud model definition object
	 * @type {Object}
	 * @see salud:SaludModel~SaludDefinition
	 */
	definition: SaludDefinition,

	/**
	 * The Salud model schema
	 * @type {MongooseSchema}
	 * @see salud:model~SaludSchema
	 */
	schema: SaludSchema,

	/**
	 * The Salud model instance
	 * @type {salud:model~Salud}
	 */
	model: Salud

};

/**
 * Validate the uniqueness of the given name
 *
 * @api private
 * @param {String} value - The username to check for uniqueness
 * @param {Function} respond - The callback function
 */
function validateUniqueName(value, respond) {
	// jshint validthis: true
	var self = this;

	// check for uniqueness of user name
	this.constructor.findOne({name: value}, function (err, salud) {
		if (err) {
			throw err;
		}

		if (salud) {
			// the searched name is my name or a duplicate
			return respond(self.id === salud.id);
		}

		respond(true);
	});
}
