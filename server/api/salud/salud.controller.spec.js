/* jshint unused:false */
'use strict';

var should = require('should');
var app = require('../../app');
var request = require('supertest');
var saludModel = require('./salud.model');

// Clear all saluds
function cleanup(done) {
	saludModel.model.remove().exec().then(function () { done();	});
}

describe('/api/consejos/salud', function () {

	var salud;

	// reset salud before each test
	beforeEach(function () {
		salud = {
			name: 'Dog',
			info: 'Hello, this is dog.',
			active: true
		};
	});

	// Clear saluds before each test
	beforeEach(cleanup);

	// Clear saluds after each test
	afterEach(cleanup);

	describe('GET', function () {

		it('should respond with JSON array', function (done) {
			request(app)
				.get('/api/consejos/salud')
				.set('Accept', 'application/json')
				.expect(200)
				.expect('Content-Type', /json/)
				.end(function (err, res) {
					if (err) {
						return done(err);
					}
					res.body.should.be.instanceof(Array);
					done();
				});
		});

		it('should respond with an error for a malformed salud id parameter', function (done) {
			request(app)
				.get('/api/consejos/salud/malformedid')
				.set('Accept', 'application/json')
				.expect(400)
				.expect('Content-Type', /json/)
				.end(done);
		});

		it('should respond with an not found error for a not existing salud id', function (done) {
			request(app)
				.get('/api/consejos/salud/cccccccccccccccccccccccc')
				.set('Accept', 'application/json')
				.expect(404)
				.expect('Content-Type', /json/)
				.end(done);
		});

		it('should return a salud for its id', function (done) {
			saludModel.model(salud).save(function (err, doc) {
				request(app)
					.get('/api/consejos/salud/' + doc._id)
					.set('Accept', 'application/json')
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function (err, res) {
						if (err) {
							return done(err);
						}
						res.body.should.be.an.Object.and.have.properties(salud);
						res.body._id.should.exist;
						done();
					});
			});
		});

	});

	describe('POST', function () {

		it('should create a new salud and respond with 201 and the created salud', function (done) {
			request(app)
				.post('/api/consejos/salud')
				.set('Accept', 'application/json')
				.send(salud)
				.expect(201)
				.expect('Content-Type', /json/)
				.end(function (err, res) {
					if (err) {
						return done(err);
					}
					res.body.should.be.an.Object.and.have.properties(salud);
					res.body._id.should.exist;
					done();
				});
		});

	});

	describe('PUT', function () {

		it('should return an error if attempting a put without an id', function (done) {
			request(app)
				.put('/api/consejos/salud')
				.set('Accept', 'application/json')
				.send(salud)
				.expect(404)
				.end(done);
		});

		it('should respond with an not found error for a not existing salud id', function (done) {
			request(app)
				.put('/api/consejos/salud/cccccccccccccccccccccccc')
				.set('Accept', 'application/json')
				.expect(404)
				.expect('Content-Type', /json/)
				.end(done);
		});

		it('should update a salud and respond with the updated salud', function (done) {
			request(app)
				.post('/api/consejos/salud')
				.set('Accept', 'application/json')
				.send(salud)
				.end(function (err, res) {
					if (err) {
						return done(err);
					}
					salud.name = 'Cat';
					// check if id is stripped on update
					salud._id = 'malformed id string';
					request(app)
						.put('/api/consejos/salud/' + res.body._id)
						.set('Accept', 'application/json')
						.send(salud)
						.expect(200)
						.expect('Content-Type', /json/)
						.end(function (err, res) {
							if (err) {
								return done(err);
							}
							res.body.should.be.an.Object.and.have.property('name', salud.name);
							done();
						});
				});
		});

	});

	describe('DELETE', function () {

		it('should return an error if attempting a delete without an id', function (done) {
			request(app)
				.delete('/api/consejos/salud')
				.set('Accept', 'application/json')
				.expect(404)
				.end(done);
		});

		it('should respond with an not found error for a not existing salud id', function (done) {
			request(app)
				.delete('/api/consejos/salud/cccccccccccccccccccccccc')
				.set('Accept', 'application/json')
				.expect(404)
				.expect('Content-Type', /json/)
				.end(done);
		});

		it('should delete a salud and respond with 204', function (done) {
			request(app)
				.post('/api/consejos/salud')
				.set('Accept', 'application/json')
				.send(salud)
				.end(function (err, res) {
					if (err) {
						return done(err);
					}
					request(app)
						.delete('/api/consejos/salud/' + res.body._id)
						.set('Accept', 'application/json')
						.expect(204)
						.end(done);
				});
		});
	});
});
