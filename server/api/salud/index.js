/**
 * Module for handling salud requests.
 * Initializing the [SaludController]{@link salud:controller~SaludController}
 * and configuring the express router to handle the salud api
 * for /api/saluds routes. All Routes are registered after the
 * [request parameters]{@link salud:parameters} have been
 * added to the router instance.
 * Exports the configured express router for the salud api routes
 * @module {express.Router} salud
 * @requires {@link module:middleware}
 * @requires {@link salud:controller~SaludController}
 */
'use strict';

var router = require('express').Router();
var contextService = require('request-context');
var middleware = require('../../lib/middleware');
var SaludController = require('./salud.controller');
var auth = require('../../lib/auth/auth.service');

// Export the configured express router for the salud api routes
module.exports = router;

/**
 * The api controller
 * @type {salud:controller~SaludController}
 */
var controller = new SaludController(router);

// register salud route parameters, uncomment if needed
// var registerSaludParameters = require('./salud.params');
// registerSaludParameters(router);

// add context for auth sensitive resources
var addRequestContext = contextService.middleware('request');

// add the authenticated user to the created acl context
var addUserContext = auth.addAuthContext('request:acl.user');

// check if the request is made by an authenticated user with at least the user role
var isAuthenticated = auth.hasRole('user');

// apply auth middleware to all routes
router.route('*').all(addRequestContext, isAuthenticated, addUserContext);

// register salud routes
router.route('/')
	.get(controller.index)
	.post(controller.create);

router.route('/' + controller.paramString)
	.get(controller.show)
	.delete(controller.destroy)
	.put(controller.update)
	.patch(controller.update);
