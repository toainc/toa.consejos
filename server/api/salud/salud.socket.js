/**
 * Module for registering broadcast updates to clients when
 * the Salud model changes. Exports the
 * [register function]{@link salud:socket~registerSaludSockets}
 * to register the model schema events on the socket instance.
 * @module {function} salud:socket
 * @requires {@link salud:model}
 */
'use strict';

/**
 * The Salud model instance
 * @type {salud:model~Salud}
 */
var Salud = require('./salud.model').model;

// export the function to register all socket broadcasts
exports.register = registerSaludSockets;

/**
 * Register Salud model change events on the passed socket
 * @param {socket.io} socket - The socket object to register the Salud model events on
 */
function registerSaludSockets(socket) {
	Salud.schema.post('save', function (doc) {
		onSave(socket, doc);
	});

	Salud.schema.post('remove', function (doc) {
		onRemove(socket, doc);
	});
}

/**
 * Emit a Salud save event on a socket object: 'salud:save'
 * @param {socket.io} socket - The socket object to emit the Salud save event on
 * @param {MogooseDocument} doc - The saved document that triggered the event
 * @param {function} cb - The callback function
 */
function onSave(socket, doc, cb) {
	socket.emit('salud:save', doc);
}

/**
 * Emit a Salud remove event on a socket object: 'salud:remove'
 * @param {socket.io} socket - The socket object to emit the Salud remove event on
 * @param {MogooseDocument} doc - The removed document that triggered the event
 * @param {function} cb - The callback function
 */
function onRemove(socket, doc, cb) {
	socket.emit('salud:remove', doc);
}
